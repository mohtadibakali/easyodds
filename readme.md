 
RESTful API Endpoint Design


Method	 End Point	                    Description


GET     /api/v1/user	                Returns an array of all user objects


GET     /api/v1//user/:id	            Returning a single user object


PUT	    /api/v1//user/:id	            Update an existing user


POST	/api/v1//user	                Create a new user


GET	    /api/v1//role	                Returns an array of all roles objects


GET	    /api/v1//role/:id	            Returning a single role object


GET	    /api/v1//order	                Returns an array of all orders objects


GET	    /api/v1//order	                Returns an array of all orders objects


GET	    /api/v1//order/:id	            Returning a single order object


PUT	    /api/v1//order/:id	            Update an existing order


GET	    /api/v1//order/status/completed	Returning an array of orders, filtered by status


GET	    /api/v1/order/user/:id	        Returning an array of all orders made by a specific user


POST    /api/v1//order	                Create a new order


GET	    /api/v1//retailer	            Returns an array of all retailer objects


GET	    /api/v1//retailer/:id	        Returning a single retailer object

  

  To view this file you first need to:
  ------------------------------------

  1) Clone the public repository: git@bitbucket.org:mohtadibakali/easyodds.git

  2) Import the file easyoddsbd.sql from EASYODD TEST folder from document root.

  3) Use the extension for chrome: Advanced REST client In order to make the request. 

  4) composer install uder easyodds folder

  5) edit .env file (db credentials)

  List of example request: 

  -> request_method = GET  

  + Returning a single object of each class 


  http://local.easyodds.com/api/v1/order/1


  http://local.easyodds.com/api/v1/role/1


  http://local.easyodds.com/api/v1/user/1


  http://local.easyodds.com/api/v1/retailer/1



  + Returning a specific user role


  http://local.easyodds.com/api/v1/role/1


  + Returning an array of all orders made by a specific user


  http://local.easyodds.com/api/v1/order/user/2


  + Returning an array of orders, filtered by status


   http://local.easyodds.com/api/v1/order/status/completed 


  OR 


  http://local.easyodds.com/api/v1/order/status/deleted
  

  -> request_method = POST

  + Create a new user


  http://local.easyodds.com/api/v1/user


    {
        "firstName": "Firt Name",
        "lastName": "Last Name",
        "email": "testing@testing.com",
        "role_id": "3",
        "password": "will be encrypted"
    }
  

  + Create a new order


  http://local.easyodds.com/api/v1/order 


    {
        "user_id": 3,
        "retailer_id": 4,
        "status": "completed",
        "total": 36.45
    }

  -> request_method = PUT

  + Update an existing user


  http://local.easyodds.com/api/v1/user/:id


      {
        "firstName": "Firt Name",
        "lastName": "Last Name",
        "email": "testing@testing.com",
        "role_id": "3",
        "password": "will be encrypted"
    }

  + Update an existing order


   http://local.easyodds.com/api/v1/order/:id


       { 
        "user_id": "2",
        "retailer_id": "1",
        "status": "Completed",
        "total": "600.51" 
        }

  

  --------
  Developed by 
  Mouhchadi Bakali Tahiri
   