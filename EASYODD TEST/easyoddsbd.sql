-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 07, 2017 at 11:41 AM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `easyoddsbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `user_id` int(2) NOT NULL,
  `retailer_id` int(2) NOT NULL,
  `status` enum('Not Assigned','In Progress','Rejected','Aborted','Completed','Cancelled','Deleted') NOT NULL,
  `total` double(7,2) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `retailer_id`, `status`, `total`, `updated_at`) VALUES
(1, 3, 5, 'Cancelled', 319.62, '0000-00-00 00:00:00'),
(2, 5, 3, 'Completed', 2528.00, '2017-03-07 11:03:12'),
(3, 5, 2, 'Completed', 99999.99, '0000-00-00 00:00:00'),
(4, 2, 1, 'Deleted', 60.51, '0000-00-00 00:00:00'),
(5, 3, 5, 'In Progress', 13.64, '0000-00-00 00:00:00'),
(6, 4, 5, 'Completed', 29.78, '0000-00-00 00:00:00'),
(7, 2, 4, 'Completed', 987.28, '0000-00-00 00:00:00'),
(8, 2, 4, 'Deleted', 987.28, '0000-00-00 00:00:00'),
(9, 3, 4, 'Completed', 298.24, '0000-00-00 00:00:00'),
(10, 3, 4, 'Completed', 298.24, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `retailers`
--

CREATE TABLE IF NOT EXISTS `retailers` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `location` varchar(150) NOT NULL,
  `email` varchar(75) NOT NULL,
  `secret` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `retailers`
--

INSERT INTO `retailers` (`id`, `name`, `location`, `email`, `secret`) VALUES
(1, 'Crooks-Gulgowski', 'Roscoetown', 'mueller.michelle@olson.com', '97'),
(2, 'Feil, Bins and Torp', 'North Jarod', 'luther75@sipes.com', '9631'),
(3, 'Parker-Haley', 'Mozellefurt', 'mable68@schinner.com', '649938'),
(4, 'Cummerata Ltd', 'Lorenzfort', 'quigley.joshuah@labadie.com', '8592'),
(5, 'Osinski, Torphy and Maggio', 'Port Vidalland', 'peichmann@jerde.com', '46'),
(6, 'GlobalCom', 'London', 'globalcom@hotmail.com', '5878952');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'level 1'),
(3, 'level 2'),
(4, 'level 3'),
(5, 'level 4'),
(6, 'level 5');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `email` varchar(70) NOT NULL,
  `password` varchar(150) NOT NULL,
  `role_id` int(2) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `email`, `password`, `role_id`, `updated_at`) VALUES
(1, 'Mahmoud', 'Cebiran', 'mono@mono.com', '$2y$10$N6L6JGNbjZJKxVQNGeutxeUQxtDr88bBeOxLNlOSd8kgEhDMPkpmO', 2, '2017-03-07 11:17:06'),
(2, 'Eldon', 'Reynolds', 'rbailey@johnston.com', 'Ve$+{(uSIErTL:)vK2[EZO7rG2N68INCq2<~4f:]x^>zz?#k;7&t!g:aXPu&^2&u3RvX3-cUVS6Chh(350=k^q7)C3', 2, '0000-00-00 00:00:00'),
(3, 'Danyka', 'Moore', 'lesch.fletcher@yahoo.com', 'k;9VEZ\\#*9d)YkKZuyBg&kSS"M\\9%_;@a[a/}Nye9GC].Wc\\UbBupGmbv2:wC6f8', 4, '0000-00-00 00:00:00'),
(4, 'Roel', 'Hammes', 'qlindgren@yahoo.com', '56p{>"xSH$^M4?P8G=0T)~`q>)(AlSrd*lcGls4`)XBJB1zN=aM!5VsD', 2, '0000-00-00 00:00:00'),
(5, 'Maia', 'Rolfson', 'turcotte.helena@hotmail.com', 'eW2`p(ORn;D4QU"Wy@,s3tVr:~+I5B5=weGe&o#~>|Y["[Z!#YyB++=f*L&u}A@W.C`T]Y.5H3:t]:Q', 3, '0000-00-00 00:00:00'),
(6, '3', 'Completed', 'juan@gabriel.com', '$2y$10$QzJazUQ5loi0TkmhlJ3lUOhjVNfDqagaKZLiErin6bF3YO7Wcu4Rm', 2, '2017-03-07 11:36:52');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
