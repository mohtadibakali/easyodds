<?php

Route::get('/',function(){
    return view("index");
});
Route::resource('about','HomeController');


Route::group(['prefix' => 'api/v1'], function () {
    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
    Route::resource('retailer', 'RetailerController');
    Route::resource('order', 'OrderController');

    Route::get('order/user/{id}','OrderController@getUserOrders');
    Route::get('order/status/{status}','OrderController@getOrdersStatus');
    Route::put('order/{id}', 'OrderController@update');
});













