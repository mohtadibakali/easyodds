<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\User;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = [
        'user_id', 'retailer_id', 'status','total'
    ];

    public static function getAllOrders(){
        $return_json ["orders"] =    Order::all();
        $return_json ["operation_status"]   = Config::get("constants.status.200");
        return $return_json;
    }


    public static function getOrderId($id){

        $found = Order::find($id);

        if(!$found){
            $return_json ["operation_status"]   = Config::get("constants.status.404");
            return $return_json;
        }else{
            $return_json ["order"] = $found;
            $return_json ["operation_status"]   = Config::get("constants.status.200");
            return $return_json;
        }


    }

    public static function createNewOrder(array $array){
        //$affected = Order::where('firstName', '=' ,$array["firstName"])->where('lastName', '=' ,$array["lastName"])->first();

        if(!User::find($array["user_id"]) || !Ratailer::find($array["retailer_id"])) {
            $return_json ["operation_status"]   = Config::get("constants.status.404");
            return $return_json;
        }else{

            DB::table('orders')->insert(
                array('user_id'	        =>	$array["user_id"],
                    'retailer_id'	    =>	$array["retailer_id"],
                    'status'	        =>	$array["status"],
                    'total'             =>  $array["total"])
            );
            return Config::get("constants.status.200");
        }
    }



    public static function getUserOrders($id){
        if(!User::find($id)) {
            $return_json ["operation_status"]   = Config::get("constants.status.404");
            return $return_json;
        }else{
            $return_json ["order"] = Order::where('user_id', '=' ,$id)->get();
            $return_json ["operation_status"]   = Config::get("constants.status.200");
            return $return_json;

        }
    }


    public static function getOrdersStatus($status){
        return Order::where('status', 'like' ,$status)->get();
    }


    public static function updateOrder(array $array, $id){
//        DB::table('orders')->where('id','=', $id)->update($array);
//        $return_json ["operation_status"]   = Config::get("constants.status.200");
//        return $return_json;
    }


}
