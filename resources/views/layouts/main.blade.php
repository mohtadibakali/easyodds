<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    @include('layouts.head')
    <title>@yield('title','hello')</title>
    <nav class="navbar">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">RESTful API Endpoint Design </a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                @if(Auth::check())
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Options <span class="caret"></span></a>
                        <ul class="dropdown-menu">

                        </ul>
                    </li>
                </ul>
                @endif

                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown">
                        <a class="navbar-brand pull-right" href="about">About </a>
                    {{--@if(!Auth::check())--}}
                        {{--<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Options <span class="caret"></span></a>--}}
                    {{--@else--}}
                        {{--<li>--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user clean"> </span></span>&nbsp;{!! Auth::user()->name !!}--}}
                                {{--<span class="caret"></span>--}}
                            {{--</a>--}}
                    {{--@endif--}}


                        <ul class="dropdown-menu">
                            {{--@if(!Auth::check())--}}
                                {{--<li><a href="/auth/login">Login</a></li>--}}
                                {{--<li><a href="/auth/register">Register</a></li>--}}
                            {{--@else--}}
                                {{--<li><a href="/auth/logout">Logout</a></li>--}}
                            {{--@endif--}}
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</head>


<body>




<div class="main_container">

        <div class="col-md-9">
            @yield('content')


        </div>

    </div>

</div>
<!-- /.container -->




<footer class="row" >
    @include('layouts.footer')
</footer>
</body>
</html>

