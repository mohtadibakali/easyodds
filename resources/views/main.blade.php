@extends('layouts.main')
@section('title','Main Page')
@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><b>Tech Acknowledgement</b></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                @if( count($classifields) < 1)
                    <div class="col-md-10">
                        <h3 class="text-center">Sorry there arent any record mathch with the criteria</h3>
                    </div>
                @endif


                @foreach($classifields as $classi)

                    <?php

                        $categoria_id = \App\Classifield::find($classi->id)->category_id;
                        $categoria_name = \App\Category::find($categoria_id)->name;
                         ?>

                    <div class="col-md-10">
                         <h4><a href="/classfields/{{$classi->id}}/"> {{$classi->title}}   {{$categoria_name }}</a> </h4>
                         <p class="text-justify">{{$classi->description}} </p>
                        <hr>
                     </div>

                @endforeach


            </div>
        </div>
    </div>
@stop