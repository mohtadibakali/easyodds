@extends('layouts.main')
@section('title','Main Page')
@section('content')

    <div class="container">

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Method</th>
                <th>End Point</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
                <tr><td>GET/</td><td>user</td><td>Returns an array of all user objects</td></tr>
                <tr><td>GET /user/</td><td>:id </td><td>Returning a single user object</td></tr>
                <tr><td>PUT </td><td>/user/:id </td><td>Update an existing user</td></tr>
                <tr><td>POST </td><td>/user </td><td>Create a new user</td></tr>
                <tr><td>GET </td><td>/role </td><td>Returns an array of all roles objects</td></tr>
                <tr><td>GET </td><td>/role/:id </td><td>Returning a single role object</td></tr>
                <tr><td>GET </td><td>/order</td><td>Returns an array of all orders objects</td></tr>
                <tr><td>GET </td><td>/order </td><td>Returns an array of all orders objects</td></tr>
                <tr><td>GET </td><td>/order/:id </td><td>Returning a single order object</td></tr>
                <tr><td>PUT </td><td>/order/:id </td><td>Update an existing order</td></tr>
                <tr><td>GET </td><td>/order/status/completed </td><td>Returning an array of orders, filtered by status</td></tr>
                <tr><td>GET </td><td>order/user/:id </td><td>Returning an array of all orders made by a specific user</td></tr>
                <tr><td>POST /</td><td>order </td><td>Create a new order</td></tr>
                <tr><td> GET </td><td>/retailer </td><td>Returns an array of all retailer objects</td></tr>
                <tr><td> GET  </td><td>/retailer/:id </td><td>Returning a single retailer object</td></tr>
            </tbody>
        </table>
    </div>
@stop
