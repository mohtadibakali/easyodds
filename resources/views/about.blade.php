@extends('layouts.main')
@section('title','Main Page')
@section('content')

    <pre class="container">
<pre>
        Technical Test – February 2017

        RESTful API Endpoint Design
        For the UML class diagram below, provide an example API endpoint design using REST
        principles.
        At a minimum, you should provide endpoints for:
        - Returning an array of all objects of each class.
        - Returning a single object of each class.
        - Returning a specific users role
        - Returning an array of all orders made by a specific user
        - Returning an array of orders, filtered by status
        - Create a new user
        - Create a new order
        - Update an existing user
        - Update an existing order
        - Manage null data responses
        - Handle not found scenario
        Please also demonstrate how you would test the API
        Please provide your response documentation in the following format:
        METHOD       /ENDPOINT    DESCRIPTION

    </pre>

    {{ HTML::image('images/uml.jpeg') }}

@stop
